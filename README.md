# Caribou-Onboard

This hacks GNOME to use [onboard](https://launchpad.net/onboard)
instead of the built-in
[caribou](https://wiki.gnome.org/Projects/Caribou) as the onscreen
keyboard.

## WARNING
This is very much work in progress and might cause all sorts of
interesting problems..

## Installing

To install this, just run
```shell
$ make install
```
and reload GNOME
 * open the command dialogue (`alt`+`F2`) and type `r`
 * `killall -3 gnome-shell`
 * logout and back in

To uninstall run either
```shell
$ make uninstall
$ gnome-extensions uninstall caribou-onboard@yannickulrich.gitlab.io
```
and reload GNOME

## How does it work?

The basic idea is based on [cariboublocker](https://github.com/lxylxy123456/cariboublocker).
There, caribou was disabled by overriding
[`_lastDeviceIsTouchscreen`](https://gitlab.gnome.org/GNOME/gnome-shell/-/blob/main/js/ui/keyboard.js#L1202).
However, this only works if the screen keyboard is turned of in a11y
```shell
$ gsettings get org.gnome.desktop.a11y.applications screen-keyboard-enabled
```
which in turn seems to cause problem with onboard (?). So instead of
overriding the detection, we now override the
[opening](https://gitlab.gnome.org/GNOME/gnome-shell/-/blob/main/js/ui/keyboard.js#L1785) and
[closing](https://gitlab.gnome.org/GNOME/gnome-shell/-/blob/main/js/ui/keyboard.js#L1821) of
carribou
```javascript
function close(){ /* hide onboard */ }
function open (){ /* show onboard */ }
KeyboardUI.Keyboard.prototype.close = close;
KeyboardUI.Keyboard.prototype.open = open;
```
We can open and close onboard using dbus
```shell
$ dbus-send --print-reply --dest=org.onboard.Onboard /org/onboard/Onboard/Keyboard org.onboard.Onboard.Keyboard.Show
$ dbus-send --print-reply --dest=org.onboard.Onboard /org/onboard/Onboard/Keyboard org.onboard.Onboard.Keyboard.Hide
```
This is *not* an elegant solution but it sort of works.
