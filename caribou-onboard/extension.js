const KeyboardUI = imports.ui.keyboard;
const Gio = imports.gi.Gio;

let originalopen;
let originalclose;
let originalmoving;

const myIface = '<node><interface name="org.onboard.Onboard.Keyboard">\
        <method name="Show">     </method>\
        <method name="Hide">     </method>\
        <method name="ToggleVisible">     </method>\
        <property name="Visible" type="b" access="read"/>\
        <property name="AutoShowPaused" type="b" access="readwrite"/>\
    </interface></node>';

const MyProxy = Gio.DBusProxy.makeProxyWrapper(myIface);

function open()
{
    console.log("Opening");
    console.log(Error().stack);
    const proxy = new MyProxy(
        Gio.DBus.session,
        'org.onboard.Onboard',
        '/org/onboard/Onboard/Keyboard', (_p, error) => {
            if (error)
                logError(error, 'Error connecting to Nautilus');
        }
    );
    proxy.ShowRemote();
}

function close()
{
    console.log("Closing");
    console.log(Error().stack);
    const proxy = new MyProxy(
        Gio.DBus.session,
        'org.onboard.Onboard',
        '/org/onboard/Onboard/Keyboard', (_p, error) => {
            if (error)
                logError(error, 'Error connecting to Nautilus');
        }
    );
    proxy.HideRemote();
}

function moving() {
    console.log("Moving");
    console.log(Error().stack);
}

function init() {

}

function enable() {
    originalclose = KeyboardUI.Keyboard.prototype.close;
    originalopen  = KeyboardUI.Keyboard.prototype.open ;
    originalmoving= KeyboardUI.Keyboard.prototype._onFocusWindowMoving;
    KeyboardUI.Keyboard.prototype.close = close;
    KeyboardUI.Keyboard.prototype.open  = open;
    KeyboardUI.Keyboard.prototype._onFocusWindowMoving  = moving;
}

function disable() {
    KeyboardUI.Keyboard.prototype.close = originalclose;
    KeyboardUI.Keyboard.prototype.open  = originalopen ;
    KeyboardUI.Keyboard.prototype._onFocusWindowMoving= originalmoving;
}
