NAME=caribou-onboard
ID=yannickulrich.gitlab.io
UID=$(NAME)@$(ID)

$(UID).shell-extension.zip: $(NAME)/extension.js $(NAME)/metadata.json
		zip -j $@ $^

install: $(NAME)@$(ID).shell-extension.zip
		gnome-extensions install --force $<
		@echo "Now reload gnome"

uninstall:
		gnome-extensions uninstall $(UID)
		@echo "Now reload gnome"

clean:
		rm *.zip
